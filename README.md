# Repositories for Compiler Lab (CS3140) - 2024 

This repository contains (as submodules) all the repositories of
students who are registered for the compilers lab.

Instruction for TA/Tutor:
 - Make a clone of this repository on GitLab, on your machine.
 - At the end of the deadline do the following

   ```bash
   git submodule update --remote # update all submodule to the latest
   git tag <YYYY-MM-DD>          # tag with the date of submission (deadline).
   ```
 - Push the commits to the remote repository on GitLab.